# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
export JAVA_HOME=/usr/java/jdk1.8.0_60/jre
export JAVA_HOME=/usr/java/jdk1.8.0_60/jdk
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk
export M2_HOME=/usr/local/apache-maven
export M2=/bin
export PATH=:/home/ec2-user/.local/bin:/home/ec2-user/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
